import React from "react";
import Layout from "../components/layout";
import Page from "../components/displayPage";
import TextArea from "../components/helpers/textareaEditor";
import Menus from "../components/helpers/pageMenus";
import axios from "axios";
export default class Pages extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: 0,
      title: "",
      text: "",
      is_published: false,
      type: "extra",
      updated_at: null,
      plus: "plus",
      newItem: {},
    };
    this.iniState = {
      title: "",
      text: "",
      is_published: false,
      type: "extra",
      updated_at: null,
      plus: "plus",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleTextareaChange = this.handleTextareaChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleMenuClick = this.handleMenuClick.bind(this);
  }
  componentDidMount() {
    let API_URL = process.env.API_URL;
    axios
      .get(`${API_URL}/page/latest`)
      .then((p) => {
        if (p.data) {
          var page = p.data;
          this.setState({
            id: page.id,
            title: page.title,
            text: page.text,
            is_published: page.is_published,
            type:page.type,
            updated_at: page.updated_at,
            add: false,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  handleChange = (e) => {
    var target = e.target;
    var value = target.type === "checkbox" ? target.checked : target.value;
    this.setState({ [target.name]: value });
  };
  handleTextareaChange = (value) => {
    this.setState({ text: value });
  };

  handleSubmit = () => {
    let API_URL = process.env.API_URL;
    var data = {
      title: this.state.title,
      text: this.state.text,
      type: this.state.type,
      is_published: this.state.is_published,
    };
    if(data.title === '' || data.text === '') return;
    axios
      .post(`${API_URL}/page/create`, data)
      .then((p) => {
        if (p.data) {
          var page = p.data;
          this.setState({
            id: page.id,
            title: page.title,
            text: page.text,
            type: page.type,
            is_published: page.is_published,
            updated_at: page.updated_at,
            add: false,
          });
          this.setState({ newItem: page });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleMenuClick = (e) => {
    var pageRef = e.target.getAttribute("pageref");
    let API_URL = process.env.API_URL;
    axios
      .get(`${API_URL}/page/findByField/ref/${pageRef}`)
      .then((p) => {
        var page = p.data;
        this.setState({
          id: page.id,
          title: page.title,
          text: page.text,
          is_published: page.is_published,
          updated_at: page.updated_at,
          type: page.type,
          add: false,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  latestPage = () => {
    let API_URL = process.env.API_URL;
    axios
      .get(`${API_URL}/page/getLatest`)
      .then((p) => {
        var page = p.data;
        this.setState({
          id: page.id,
          title: page.title,
          text: page.text,
          is_published: page.is_published,
          type:page.type,
          updated_at: page.updated_at,
          add: false,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  addPageForm = () => {
    return (
      <form className="collapse" id="page-form">
        <label>Title</label>
        <input
          type="text"
          className="form-control mb-2"
          name="title"
          value={this.state.title}
          required={true}
          onChange={this.handleChange}
        />
        <label>Body</label>
        <TextArea
          name="text"
          value={this.state.text}
          onChange={this.handleTextareaChange}
        />
        <label>Type</label>
        <select
          name="type"
          value={this.state.type}
          onChange={this.handleChange}
          className="form-control"
        >
          <option value="main">Main</option>
          <option value="common">Common</option>
          <option value="extra">Extra</option>
        </select>

        <div className="row mt-1">
          <div className="col">
            <div className="form-group form-check">
              <input
                type="checkbox"
                className="form-check-input"
                name="is_published"
                value={this.state.is_published}
                onChange={this.handleChange}
                id="is_published"
              />
              <label className="form-check-label" htmlFor="is_published">
                Publish
              </label>
            </div>
          </div>
        </div>
        <div
          className="btn-group mt-2 toggle"
          role="group"
          data-toggle="collapse"
          href="#page-form"
          aria-expanded="false"
          aria-controls="page-form"
          onClick={() => {
            this.iniState.plus = this.state.plus === "plus" ? "minus" : "plus";
            var initialData = this.iniState;
            this.setState(initialData);
          }}
        >
          <button
            type="button"
            onClick={this.handleSubmit}
            className="btn btn-success"
          >
            <i className="fa fa-save"></i>
          </button>
          <button type="button" className="btn btn-danger">
            <i className="fa fa-close"></i>
          </button>
        </div>
      </form>
    );
  };

  render() {
    var newItem = this.state.newItem;
    return (
      <Layout>
        <div className="row">
          <div className="col-md-2">
            <div className="mt-1 mt-2 mb-1"></div>
            <div className="grid">
              <Menus item={newItem} handleMenuClick={this.handleMenuClick} />
            </div>
          </div>
          <div className="col-md-10">
            <div className="card mt-2">
              <div
                className="card-body"
                data-toggle="collapse"
                href="#page-form"
                aria-expanded="false"
                aria-controls="page-form"
                onClick={() => {
                  this.iniState.plus =
                    this.state.plus === "plus" ? "minus" : "plus";
                  var initialData = this.iniState;
                  this.setState(initialData);
                }}
              >
                <i className={`fa fa-${this.state.plus}`}></i> Add
              </div>
            </div>
            {this.addPageForm()}
            {this.state.id ? (
              <Page
                id={this.state.id}
                title={this.state.title}
                text={this.state.text}
                is_published={this.state.is_published}
                type={this.state.type}
                updated_at={this.state.updated_at}
              />
            ) : (
              ""
            )}
          </div>
        </div>
      </Layout>
    );
  }
}

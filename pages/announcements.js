import React from "react";
import Layout from "../components/layout";
import AnnList from "../components/announcements";
export default class Announcements extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Layout>
        <AnnList />
      </Layout>
    );
  }
}

import Layout from "../components/layout";
import SelectedUsers from "../components/selectedUsers";
export default function TablesList() {
  return (
    <Layout>
      <div className="row">
        <div className="col-md-8 mx-auto">
          <div className="mt-1 mt-2"></div>
          <h1 className="title mx-auto">Selected Users</h1>
            <div className="card-body">
              <SelectedUsers/>
            </div>
        </div>
      </div>
    </Layout>
  );
}

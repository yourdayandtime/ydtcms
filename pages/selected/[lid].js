import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import axios from "axios";
//import parseHtml from "html-react-parser";
import Layout from "../../components/layout";
const SelectedList = () => {
  const [list, setList] = useState([]);
  const router = useRouter();
  const { lid } = router.query;
  // Similar to componentDidMount and componentDidUpdate:
  useEffect(() => {
    var url = `${process.env.API_URL}/selectedUsers/findByField/id/${lid}`;
    axios
      .get(url)
      .then((list) => {
        console.log(list);
        setList(list.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [lid, setList]);
  if (typeof list !== "object") return <div>Loading...</div>;
  var html = "";
  if (list.rows && list.rows.length) {
    var i = 1;
    html = list.rows.map((row, k) => (
      <tr key={k}>
        <th scope="row">{i++}</th>
        <td>{row.fullName}</td>
        <td>{row.genderString}</td>
        <td>{row.country}</td>
      </tr>
    ));
  }
  return (
    <Layout>
      <div className="row">
        <div className="col-md-3">
          <ul className="list-group">
            <li className="list-group-item">
              <Link href="/selected/1">
                <a>Menu One</a>
              </Link>
              </li>
              <li className="list-group-item">
              <Link href="/selected/2">
                <a>Menu Two</a>
              </Link>
              </li>
              <li className="list-group-item">
              <Link href="/selected/3">
                <a>Menu Three</a>
              </Link>
              </li>
              <li className="list-group-item">
              <Link href="/selected/4">
                <a>Menu Four</a>
              </Link>
            </li>
          </ul>
        </div>
        <div className="col-md-9 mx-auto">
          <div className="card">
            <div className="card-header">
              {list.title ? list.title : "No title provided"}
            </div>
            <div className="card-body">
              <div className="alert alert-success">
                {list.reason ? list.reason : "No description provided"}
              </div>
            </div>
          </div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Full Name</th>
                <th scope="col">Gender</th>
                <th scope="col">Country</th>
              </tr>
            </thead>
            <tbody>{html}</tbody>
          </table>
        </div>
      </div>
    </Layout>
  );
};

export default SelectedList;

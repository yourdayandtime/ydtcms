import Link from 'next/link'
import Layout from "../components/layout";
import Signin from "../components/signin";
import Refugees from "../components/dataTable";
const USER = true;
export default function Home() {
  if (!USER) return <Signin />;
  return (
    <Layout>
      <div className="row">
        <div className="col-md-2">
          <ul className="list-group">
            <li
              className=" list-group-item mb-2 mt-2"
              data-toggle="collapse"
              href="#renew-status-filter"
              aria-expanded="false"
              aria-controls="renew-status-filter"
            >
              Renew
              <i className="fa fa-list float-right mr-2"></i>
            </li>

            <ul
              className="list-group main-sidebar status collapse"
              id="renew-status-filter"
            >
              <li className="list-group-item user" data-status="this week">
                This week
              </li>
              <li className="list-group-item user" data-status="next week">
                Next week
              </li>
              <li className="list-group-item user" data-status="this month">
                This month
              </li>
              <li className="list-group-item user" data-status="expired">
                Expired
              </li>
              <li
                className="list-group-item font-weight-bold"
                data-toggle="collapse"
                href="#region-filter"
                aria-expanded="false"
                aria-controls="region-filter"
              >
                Regions <i className="fa fa-globe float-right mr-2"></i>
              </li>
              <ul className="list-group collapse" id="region-filter">
                <li className="list-group-item user" data-status="asia">
                  Asia
                </li>
                <li className="list-group-item user" data-status="north africa">
                  North Africa
                </li>
                <li className="list-group-item user" data-status="sadc">
                  SADC
                </li>
              </ul>

              <li className="list-group-item user mb-2" data-status="all">
                All
              </li>
            </ul>
                        <ul className="list-group mb-2"><li className="list-group-item"><Link href="/selected/0"><a>Saved lists</a></Link></li></ul>
            <ul className="list-group mb-2"><li className="list-group-item"><Link href="/announcements"><a>Updates</a></Link></li></ul>
            <ul className="list-group mb-2"><li className="list-group-item"><Link href="/pages"><a>Pages</a></Link></li></ul>
            <ul className="list-group mb-2"><li className="list-group-item"><Link href="/question-answer"><a>Q & A</a></Link></li></ul>
            <ul className="list-group mb-2"><li className="list-group-item"><Link href="/centres"><a>Centres</a></Link></li></ul>
          </ul>
        </div>
        <div className="col-md-10">
          <h1 className="header mt-3">Refugees & Asylum seekers</h1>
          {<Refugees />}
        </div>
      </div>
    </Layout>
  );
}

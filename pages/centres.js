import React from "react";
import Layout from "../components/layout";
import RefugeeCentres from "../components/refugeeCenters";
export default class Centres extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  
  render() {
    return (
      <Layout>
        <div className="row">
          <div className="col-md-8 mx-auto">
            <div className="mt-1 mt-2"></div>
            <h1 className="title mx-auto">Refugee Centres</h1>
            <div className="grid">
               <RefugeeCentres/>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}
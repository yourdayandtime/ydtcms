export default function Logo() { 
  return (
    <span className="border-bottom border-dark">
      <span className="logo-text-y">Y</span>
      <span className="logo-text-d">D</span>
      <span className="logo-text-t">T</span>
    </span>
  );
}

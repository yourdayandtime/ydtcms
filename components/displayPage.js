import React from "react";
import TextArea from "./helpers/textareaEditor";
import parseHtml from "html-react-parser";
import axios from "axios";
import DisplayDate from "../components/helpers/displayDate";
export default class Page extends React.Component {
  constructor(props) {
    super(props);
    this.iniValues = {
      id: this.props.id,
      title: this.props.title,
      text: this.props.text,
      is_published: this.props.is_published,
      type: this.props.type,
      updated_at: this.props.updated_at,
      edit: false,
      field: "",
    };
    this.state = this.iniValues;
    this.handleChange = this.handleChange.bind(this);
    this.handleTextareaChange = this.handleTextareaChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    console.log(this.state.updated_at);
  }

  handleChange = (e) => {
    var target = e.target;
    var value = target.type === "checkbox" ? target.checked : target.value;
    this.setState({ [target.name]: value });
  };
  handleTextareaChange = (value) => {
    this.setState({ text: value });
  };
  handleSubmit = () => {
    console.log("Submitted data");
    var newData = {
      id: this.state.id,
      title: this.state.title,
      text: this.state.text,
      is_published: this.state.is_published,
      type: this.state.type,
    };

    let API_URL = process.env.API_URL;
    axios
      .put(`${API_URL}/page/update`, newData)
      .then((p) => {
        console.log(p.data);
        this.setState({ edit: false });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  static getDerivedStateFromProps(props, state) {
    if (props.id !== state.id) {
      return {
        id: props.id,
        title: props.title,
        text: props.text,
        is_published: props.is_published,
        type: props.type,
      };
    }
    return null;
  }
  render() {
    var html = "";
    var checkBox = (
      <div className="form-group form-check">
        <input
          type="checkbox"
          className="form-check-input"
          name="is_published"
          value={this.state.is_published}
          onChange={this.handleChange}
          checked={this.state.is_published}
          id="is_published"
        />
        <label className="form-check-label" htmlFor="is_published">
          Publish
        </label>
      </div>
    );
    {
      this.state.edit
        ? (html = (
            <form>
              <label>Title</label>
              <input
                type="text"
                className="form-control mb-2"
                name="title"
                value={this.state.title}
                required={true}
                onChange={this.handleChange}
              />
              <label>Body</label>
              <TextArea
                name="text"
                value={this.state.text}
                onChange={this.handleTextareaChange}
              />
              <label>Type</label>
              <select
                className="form-control"
                name="type"
                value={this.state.type}
                onChange={this.handleChange}
              >
                <option value="extra">Extra</option>
                <option value="main">Main</option>
                <option value="common">Common</option>
              </select>
              {checkBox}
            </form>
          ))
        : (html = (
            <div>
              <div className="row">
                <div className="col">
                  <h1>{this.state.title}</h1>
                  <DisplayDate date={this.state.updated_at} />
                </div>
              </div>
              <div className="row mt-3">
                <div className="col">{parseHtml(this.state.text)}</div>
              </div>
            </div>
          ));
    }
    return (
      <div>
        {html}
        <div className="row">
          <div className="col">
            {this.state.edit ? (
              <div
                className="btn-group mt-2"
                role="group"
                aria-label="Basic example"
              >
                <button
                  type="button"
                  onClick={this.handleSubmit}
                  className="btn btn-success"
                >
                  <i className="fa fa-save"></i>
                </button>
                <button
                  type="button"
                  onClick={() => {
                    this.setState({ edit: !this.state.edit });
                  }}
                  className="btn btn-danger"
                >
                  <i className="fa fa-close"></i>
                </button>
              </div>
            ) : (
              <div className="row">
                <div className="col small">
                  Published{" "}
                  {this.state.is_published ? (
                    <i className="text-success fa fa-check"></i>
                  ) : (
                    <i className="text-danger fa fa-close"></i>
                  )}
                </div>
                <div className="col">
                  <span>Type: {this.state.type}</span>
                </div>
                <div className="col">
                  <i
                    className="fa fa-pencil float-right p-2"
                    onClick={() => {
                      this.setState({
                        edit: !this.state.edit,
                      });
                    }}
                  ></i>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";
import dynamic from "next/dynamic";
const QuillNoSSRWrapper = dynamic(import("react-quill"), {
  ssr: false,
  loading: () => <p>Loading ...</p>,
});
class TextareaEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {value: this.props.value };
  }

  modules = {
    toolbar: [
      [{ header: [1, 2, 3, 4, false] }],
      ["bold", "italic", "underline", "blockquote"],
      [
        { list: "ordered" },
        { list: "bullet" },
        { indent: "-1" },
        { indent: "+1" },
      ],
      [
        { align: "" },
        { align: "center" },
        { align: "right" },
        { align: "justify" },
      ]["clean"], ['link', 'image']
    ],
  };

  formats = [
    "header",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "align",
  ];
  render() {
    return (
        <QuillNoSSRWrapper
          modules={this.modules}
          formats={this.formats}
          name={this.props.name}
          value={this.props.value}
          required
          onChange={this.props.onChange}
        />
    );
  }
}
export default TextareaEditor;

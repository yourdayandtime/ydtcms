export default function SaveCancelBtns(props) {
  return (
    <div className="btn-group mt-2" role="group">
      <button
        type="button"
        onClick={props.handleSubmit}
        className="btn btn-success"
      >
        <i className="fa fa-save"></i>
      </button>
      <button
        type="button"
        onClick={props.handleCancel}
        className="btn btn-danger"
      >
        <i className="fa fa-close"></i>
      </button>
    </div>
  );
}

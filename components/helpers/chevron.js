import React ,{useState} from 'react';
export default function chevron(props) {
    const [upDown, setUpDown] = useState('down');
    return  <i onClick={() => {setUpDown(upDown === 'up' ? 'down' : 'up')}} className={'p-2 fa fa-chevron-'+upDown+ ' '+props.position}></i>
}
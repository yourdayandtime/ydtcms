import React from "react";
import axios from "axios";
import parseHtml from "html-react-parser";
import Editor from "./textareaEditor";
import SaveCancelBtns from "./saveCancelBtns";
import DisplayDate from './displayDate';
class DisplayCenter extends React.Component {
  constructor(props) {
    super(props);
    this.iniData = { center: this.props.center, error: false, edit: false };
    this.state = this.iniData;
    this.editForm = this.editForm.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleEditorChange = this.handleEditorChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleEditorChange = (value) => {
    var center = this.state.center;
    center.text = value;
    this.setState({ center: center });
  };

  handleCancel = () => {
    let center = this.state.center;
    let centerText = this.iniData.center.text;
    center.text = centerText;
    this.setState({ center: center, edit: false });
  };

  handleSubmit = () => {
    let API_URL = process.env.API_URL;
    axios
      .put(`${API_URL}/center/update`, this.state.center)
      .then((c) => {
        this.iniData.center = c.data.Center;
        this.setState({iniData: c.data.Center, center: c.data.Center, edit: false });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  editForm = () => {
    return (
      <form>
        <Editor
          name="text"
          value={this.state.center.text}
          onChange={this.handleEditorChange}
        />
        <SaveCancelBtns
          handleSubmit={this.handleSubmit}
          handleCancel={this.handleCancel}
        />
      </form>
    );
  };
  componentWillUnmount() {}

  render() {
    if (!this.state.center) {
      return <div />;
    }
    var c = this.state.center;
    return (
      <div className="card">
        <div className="card-body">
          <h2 className={"font-weight-bold text-" + c.color_cls}>{c.name}</h2>
          {this.state.edit ?  (this.editForm()) : ( <div>{parseHtml(this.state.center.text ? this.state.center.text : '')}</div>)}
          {!this.state.edit ? 
              (<div className="row">
                <div className="col small">
                  <DisplayDate date={this.state.center.updated_at}/>
                </div>
                <div className="col">
                  <i
                    className="fa fa-pencil float-right p-2"
                    onClick={() => {
                      this.setState({
                        edit: !this.state.edit,
                      });
                    }}
                  ></i>
                </div>
              </div>
            ):("")}
        </div>
      </div>
    );
  }
}

export default DisplayCenter;

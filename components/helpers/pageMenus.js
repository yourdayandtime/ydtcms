import React from "react";
import axios from "axios";
class PagesListMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = { menus: [], newItem: {} };
  }
  static getDerivedStateFromProps(props, state) {
    if (state.menus.length) {
      if (state.menus[0].id !== props.item.id) {
        var currMenus = state.menus;
        currMenus.unshift(props.item);
        return currMenus;
      }
      return null;
    } else {
      var currMenus = state.menus;
      currMenus.push(props.item);
      return currMenus;
    }
  }
  componentDidMount() {
    let API_URL = process.env.API_URL;
    axios
      .get(`${API_URL}/page/getMenus`)
      .then((m) => {
        if (m.data) {
          this.setState({ menus: m.data });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    if (!this.state.menus.length) return <div>Loading menus...</div>;
    var menus = this.state.menus;
    var main = [];
    var common = [];
    var extra = [];
    for (var i = 0; menus.length > i; i++) {
      var li = (
        <li
          key={i}
          className="list-group-item"
          pageid={menus[i]["id"]}
          pageref={menus[i]["ref"]}
          onClick={this.props.handleMenuClick}
        >
          {menus[i]["title"]}
        </li>
      );
      if (menus[i]["type"] === "main") {
        main.push(li);
      } else if (menus[i]["type"] === "common") {
        common.push(li);
      } else {
        extra.push(li);
      }
    }
    return (
      <div>
        <ul className="list-group">
          <li
            className="list-group-item"
            data-toggle="collapse"
            href="#collapse-main"
            role="button"
            aria-expanded="false"
            aria-controls="collapse-main"
          >
            Main <i className="fa fa-chevron-down float-right"></i>
            <ol className="list-group collapse mt-1" id="collapse-main">
              {main.length
                ? main.map((li) => {
                    return li;
                  })
                : "No page added"}
            </ol>
          </li>
          <li
            className="list-group-item"
            data-toggle="collapse"
            href="#collapse-common"
            role="button"
            aria-expanded="false"
            aria-controls="collapse-common"
          >
            Common <i className="fa fa-chevron-down float-right"></i>
            <ol className="list-group mt-1 collapse" id="collapse-common">
              {common.length
                ? common.map((li) => {
                    return li;
                  })
                : "No page added"}
            </ol>
          </li>
          <li
            className="list-group-item mb-4"
            data-toggle="collapse"
            href="#collapse-extra"
            role="button"
            aria-expanded="false"
            aria-controls="collapse-extra"
          >
            Extra <i className="fa fa-chevron-down float-right"></i>
            <ol className="list-group collapse mt-1" id="collapse-extra">
              {extra.length
                ? extra.map((li) => {
                    return li;
                  })
                : "No page added"}
            </ol>
          </li>
        </ul>
      </div>
    );
  }
}
export default PagesListMenu;

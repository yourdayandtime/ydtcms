import React from "react";
import axios from "axios";
const $ = require("jquery");
$.DataTable = require("datatables.net");
/* require( 'jszip' );
require( 'pdfmake' ); loaded via cdn*/
require("datatables.net-buttons-dt");
//require("datatables.net-buttons/js/buttons.colVis.js");
//require("datatables.net-buttons/js/buttons.flash.js");
require("datatables.net-buttons/js/buttons.html5.js");
require("datatables.net-buttons/js/buttons.print.js");
require("datatables.net-select-dt");
//require("datatables.net-searchpanes");

const columns = [
  { title: "Name", data: "fname" },
  { title: "Surname", data: "lname" },
  { title: "Gender", data: "genderString" },
  { title: "Permit type", data: "permit_type" },
  { title: "Permit no", data: "permit_no" },
  { title: "Exp date", data: "exp_date" },
  { title: "Renew", data: "exp_status" },
  { title: "Country", data: "country" },
  { title: "Region", data: "region" },
];
class DataTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: [], error: 0 };
    this.selectPersistedRows = this.selectPersistedRows.bind(this);
    this.persistSelection = this.persistSelection.bind(this);
    this.saveListData = this.saveListData.bind(this);
  }
  componentDidMount() {
    //To do: find way to avoid multiple requests of same data from the server/api and reuse previously received data
    axios
      .get(`${process.env.API_URL}/user/`)
      .then((users) => {
        console.log(users.data);
        var table = $("#refugees_table").DataTable({
          //dom: "Bfrtip",//required to display
          dom: 'B<"clear">lfrtip', // this allows to show entries select box to display when other buttons dispay
          data: users.data,
          columns,
          stateSave: true, //keep current data on reload
          buttons: [
            "print",
            "excel",
            "pdf",
            {
              text: "Save",
              action: function () {
                var selectedRows = table.rows(".selected").data().pluck("id");
                //trigger click to open modal for list title and description inputs
                //the actual saving will happen when modal form is submitted
                $("button#save-list").trigger("click");
                if (selectedRows.count() === 0) {
                  alert("No data selected");
                  return;
                } else {
                  var userIds = [];
                  var modalForm = $("form#save-list-form");
                  $(modalForm).find("span.title").text("");
                  $(modalForm).find("span.desc").text("");
                  //populate ids
                  for (var i = 0; i < selectedRows.count(); i++) {
                    userIds.push(selectedRows[i]);
                  }
                  $(modalForm).find("#user_ids").data("userIds", userIds);
                }
              },
            },
          ],
          //this allow multi rows select without holding down ctr key
          select: { items: "multi" },
        });

        //hack search by keywork stored in data-status with click
        $("ul li.user ").on("click", function (e) {
          if ($(this).data("status") === "all") {
            table.search("").draw();
          } else {
            table.search($(this).data("status")).draw();
          }
        });
        //keep selected rows  highlighted
        $("#refugees_table tbody").on("click", "tr", function () {
          $(this).toggleClass("selected");
        });
        //hide renew status column
        table.column(6).visible(false);
        this.setState({ data: users.data });
        //handleSubmition of list
        var form = $("form#save-list-form");
        $(form).on("click", "button#submit-list", function () {
          var titleSpan = $(form).find("span.title");
          var descSpan = $(form).find("span.desc");
          var title = $(form).find('input[name="title"]').val().trim();
          var desc = $(form).find('textarea[name="reason"]').val().trim();
          var userIds = $(form).find("#user_ids").data("userIds");
          var formOk = true;
          if (!title.length) {
            formOk = false;
            $(titleSpan).text("Please add list title");
          }
          if (!desc.length) {
            formOk = false;
            $(descSpan).text("Please add list description");
          }
          if (formOk === false) return;
          if (userIds && typeof userIds === "object") {
            if (title.length && desc.length) {
              //send data to api for storage
              axios
                .post(`${process.env.API_URL}/selectedUsers/create`, {
                  title: title,
                  reason: desc,
                  user_ids: JSON.stringify(userIds),
                })
                .then((saved) => {
                  console.log(saved);
                  table.rows().deselect();
                  $(form).find("#user_ids").data("userIds", undefined);
                  $(form).find('input[name="title"]').val('');
                  $(form).find('textarea[name="reason"]').val()
                  $(form).find("button.close-modal").trigger("click");
                })
                .catch((err) => console.log(err));
            }
          }
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  saveListData = (title, description, ids, table, form) => {
    axios
      .post(`${process.env.API_URL}/selectedUsers/create`, {
        title: title,
        reason: description,
        user_ids: JSON.stringify(ids),
      })
      .then((saved) => {
        console.log(saved);
        table.rows().deselect();
        $(form).find("#user_ids").data("userIds", undefined);
        $(form).reset().find(".close-modal").trigger("click");
      })
      .catch((err) => console.log(err));
  };

  selectPersistedRows = (table) => {
    if (!sessionStorage.rowKeyStore) return;
    var rowKeys = JSON.parse(sessionStorage.rowKeyStore);
    for (var key in rowKeys) {
      $(table.row(key).node()).addClass("selected");
    }
  };

  persistSelection = (index, isSelected) => {
    var ss = sessionStorage;
    if (!ss.rowKeyStore) {
      ss.rowKeyStore = "{}";
    }
    var rowKeys = JSON.parse(ss.rowKeyStore);
    if (isSelected === false && rowKeys.hasOwnProperty(index)) {
      console.log("removing row " + index + " from selection list");
      delete rowKeys[index];
    } else if (isSelected) {
      rowKeys[index] = true;
    }
    ss.rowKeyStore = JSON.stringify(rowKeys);
  };
  componentWillUnmount() {
    $("#refugees_table_wrapper").find("table").DataTable().destroy(true);
  }
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <div>
        <table id="refugees_table" className="display table" />
        <button
          type="button"
          className="btn btn-primary"
          data-toggle="modal"
          data-target="#save-list-modal"
          id="save-list"
          hidden={true}
        >
          Save List Modal
        </button>
        <div className="modal" id="save-list-modal" tabIndex="-1" role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Add List Information</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form id="save-list-form">
                  <label>Title</label>
                  {" : "}
                  <span className="small title text-danger"></span>
                  <input
                    type="text"
                    name="title"
                    onChange={() => {
                      console.log("title changed");
                    }}
                    className="form-control mb-3"
                  />
                  <label>Description</label>
                  {" : "}
                  <span className="small desc text-danger"></span>
                  <textarea
                    name="reason"
                    className="form-control mt-2 mb-3"
                    onChange={() => {
                      console.log("description changed");
                    }}
                  />
                  <input type="text" hidden={true} id="user_ids" />
                  <div className="float-right">
                    <button
                      type="button"
                      className="btn btn-secondary close-modal"
                      data-dismiss="modal"
                    >
                      Close
                    </button>
                    <button
                      type="button"
                      id="submit-list"
                      className="btn btn-success ml-1"
                    >
                      Submit
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DataTable;

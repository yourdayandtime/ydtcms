import React from "react";
import Head from "next/head";
import Link from "next/link";
import Brand from "../utilities/logoText";
function Layout({ children }) {
  return (
    <React.Fragment>
      <Head>
        <title>YDT - Your Day and Time</title>
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
          crossOrigin="anonymous"
        />
        <link
          rel="stylesheet"
          href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"
        />
        <link
          rel="stylesheet"
          href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css"
        />
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
        />
        <link
          href="https://cdn.quilljs.com/1.3.6/quill.snow.css"
          rel="stylesheet"
        />
        <link href="/css/main.css" rel="stylesheet"></link>
      </Head>
      <header className="">
        <nav className="navbar navbar-expand-md navbar-light fixed-top bg-light shadow">
          <Link href="/">
            <a className="navbar-brand font-weight-bold border border-secondary rounded p-1">
              <span className="border-bottom border-dark">
                <Brand />
              </span>
            </a>
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarCollapse"
            aria-controls="navbarCollapse"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarCollapse">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link href="/">
                  <a className="nav-link">
                    <i className="fa fa-home"></i> Home
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/profile">
                  <a className="nav-link">
                    <i className="fa fa-user"></i>
                  </a>
                </Link>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <main role="main" className="container-fluid">
        {children}
      </main>
      <footer className="footer border-top">
        <ul className="nav justify-content-center">
          <li className="nav-item">
            <span className="nav-link">
              YDT &copy; {new Date().getFullYear()} Ltd.
            </span>
          </li>
        </ul>
      </footer>

      <script
        src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossOrigin="anonymous"
      ></script>
      <script
        src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossOrigin="anonymous"
      ></script>
      <script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossOrigin="anonymous"
      ></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
      <script src="/js/custom.js" />
    </React.Fragment>
  );
}

export default Layout;

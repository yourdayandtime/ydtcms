import React from "react";
import axios from "axios";
import Question from "../components/question";
import TextEditor from "../components/helpers/textareaEditor";
class Questions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: [],
      id:0,
      question: "",
      answer: "",
      error: 0,
      showAnswer: false,
      plus: "plus",
      is_published: false,
    };
    this.toggleShow = this.toggleShow.bind(this);
    this.addQABtn = this.addQABtn.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleTextEditorChange = this.handleTextEditorChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  toggleShow = () => {
    this.setState({ showAnswer: !this.state.showAnswer });
  };
  handleChange = (e) => {
    var target = e.target;
    var name = target.name;
    var value = target.type === "checkbox" ? target.checked : target.value;
    this.setState({ [name]: value });
  };
  handleTextEditorChange = (value) => {
    this.setState({ answer: value });
  };
  handleSubmit = () => {
    let API_URL = process.env.API_URL;
    let data = {
      id:this.state.id,//this should be > 0 on editing 
      question:this.state.question,
      answer:this.state.answer,
      is_published:this.state.is_published
    };
    axios
      .post(`${API_URL}/question/save`, data) //handle create and update
      .then((q) => {
        var question = q.data;
        var questions = this.state.questions;
        questions.unshift(question); //add item at the begening of the list
        this.setState({ questions: questions });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  componentDidMount() {
    let API_URL = process.env.API_URL;
    axios
      .get(`${API_URL}/question/`) //get all questions
      .then((qs) => {
        this.setState({ questions: qs.data.rows });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  componentWillUnmount() {}
  addQABtn = () => {
    return (
      <li
        className="list-group-item mb-2 toggle"
        data-toggle="collapse"
        href="#qa-form"
        aria-expanded="false"
        aria-controls="qa-form"
        onClick={() => {
          this.setState({
            plus: this.state.plus === "plus" ? "minus" : "plus",
          });
        }}
      >
        <i className={"fa fa-" + this.state.plus}></i> Add
      </li>
    );
  };
  qAForm = () => {
    return (
      <form id="qa-form" className="collapse mt-3 mb-3">
        <label>Question</label>
        <input type="text" name="question" value={this.state.question} onChange={this.handleChange} className="form-control" />
        <label>Answer</label>
        <TextEditor
          name="answer"
          value={this.state.answer}
          onChange={this.handleTextEditorChange}
        />
        <div className="row mt-2">
          <div className="col">
            <div className="form-group form-check">
              <input
                type="checkbox"
                className="form-check-input"
                name="is_published"
                value={this.state.is_published}
                onChange={this.handleChange}
                id="is_published"
              />
              <label className="form-check-label" htmlFor="is_published">
                Publish
              </label>
            </div>
          </div>
        </div>
        <div
          className="btn-group mt-2"
          role="group"
          data-toggle="collapse"
          href="#qa-form"
          aria-expanded="false"
          aria-controls="qa-form"
          onClick={() => {
            var plusSign = this.state.plus === "plus" ? "minus" : "plus";
            this.setState({ plus: plusSign });
          }}
        >
          <button
            type="button"
            onClick={this.handleSubmit}
            className="btn btn-success"
          >
            <i className="fa fa-save"></i>
          </button>
          <button type="button" className="btn btn-danger">
            <i className="fa fa-close"></i>
          </button>
        </div>
      </form>
    );
  };

  render() {
    if (!this.state.questions.length) {
      return <div />;
    }
    var html = "";
    html = this.state.questions.map((q, k) => (
      <Question key={k} question={q} />
    ));
    return (
      <div>
        <ul className="list-group">
          {this.addQABtn()}
          {this.qAForm()}
          {html}
        </ul>
      </div>
    );
  }
}

export default Questions;

import React from "react";
import parseHtml from "html-react-parser";
import DiplayDateHelper from "./helpers/displayDate";
import TextEditor from "./helpers/textareaEditor";
import axios from "axios";
export default class Announcement extends React.Component {
  constructor(props) {
    super(props);
    this.iniData = {
      id: this.props.value.id,
      title: this.props.value.title,
      text: this.props.value.text,
      is_published: this.props.value.is_published,
      edit: false,
    };
    this.state = this.iniData;
    this.handleTextareaChange = this.handleTextareaChange.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleTextareaChange = (value) => {
    this.setState({ text: value });
  };
  handleChange = (e) => {
    var target = e.target;
    var value = target.type === "checkbox" ? target.checked : target.value;
    this.setState({ [target.name]: value });
  };
  handleSubmit = () => {
    let API_URL = process.env.API_URL;
    axios
      .put(`${API_URL}/announcement/update`, this.state)
      .then((a) => {
        this.setState({ edit: false, date: a.data.updated_at });
        this.iniData = this.state;
        console.log(a);
      })
      .catch((err) => {
        console.log(err);
        this.setState({ edit: false });
      });
  };
  render() {
    if (this.state.edit)
      return (
        <div className="card">
          <div className="card-body">
            <form>
              <label>Title</label>
              <input
                className="form-control mb-2"
                type="text"
                name="title"
                value={this.state.title}
                onChange={this.handleChange}
              />
              <label>Message</label>
              <div className="mb-2">
                <TextEditor
                  name="text"
                  value={this.state.text}
                  onChange={this.handleTextareaChange}
                />
              </div>

              <div className="form-group form-check">
                <input
                  type="checkbox"
                  className="form-check-input"
                  name="is_published"
                  value={this.state.is_published}
                  onChange={this.handleChange}
                  checked={this.state.is_published}
                  id="is_published"
                />
                <label className="form-check-label" htmlFor="is_published">
                  Publish
                </label>
              </div>
              <div className="btn-group mt-2" role="group">
                <button
                  type="button"
                  onClick={this.handleSubmit}
                  className="btn btn-success"
                >
                  <i className="fa fa-save"></i>
                </button>
                <button
                  type="button"
                  onClick={() => {
                    this.setState(this.iniData);
                  }}
                  className="btn btn-danger"
                >
                  <i className="fa fa-close"></i>
                </button>
              </div>
            </form>
          </div>
        </div>
      );
    return (
      <div className="card">
        <div className="card-body">
          <div className="announcement-title mb-2">
            <b>{parseHtml(this.state.title)}</b>
          </div>
          <div className="small mutted mb-2">
            Updated: <DiplayDateHelper date={this.props.value.updated_at} />
          </div>
          <div className="announcement-text">{parseHtml(this.state.text)}</div>
          <div className="row">
            <div className="col small">
              Published{" "}
              {this.state.is_published ? (
                <i className="fa fa-check text-success"></i>
              ) : (
                <i className="fa fa-close text-danger"></i>
              )}
            </div>

            <div className="col">
              <i
                className="fa fa-pencil small text-muted p-2 float-right"
                onClick={() => {
                  this.setState({ edit: true });
                }}
              ></i>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";
import axios from "axios";
import TextArea from "./helpers/textareaEditor";
import DispalyAnnouncement from "./displayAnnouncement";
import $ from 'jquery';
class Announcements extends Component {
  constructor(props) {
    super(props);
    this.iniState = {
      anns: [],
      user_id: 1,
      text: "",
      title: "",
      is_published: false,
      plus: "plus",
    };
    this.state = this.iniState;
    this.handleChange = this.handleChange.bind(this);
    this.handleEditorChange = this.handleEditorChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    let API_URL = process.env.API_URL;
    axios
      .get(`${API_URL}/content/announcements`)
      .then((result) => {
        if (result.data) {
          this.setState({ anns: result.data });
        }
        console.log(this.state.anns);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  handleChange = (e) => {
    var target = e.target;
    var name = target.name;
    var value = target.type === "checkbox" ? target.checked : target.value;
    this.setState({ [name]: value });
  };
  handleEditorChange = (value) => {
    this.setState({ text: value });
  };
  handleSubmit = () => {
    if (this.state.text === "") return;
    console.log("ann frm submited");
    let API_URL = process.env.API_URL;
    axios
      .post(`${API_URL}/announcement/create`, this.state)
      .then((result) => {
        if (result.data) {
          //console.log(result);
          var anns = this.state.anns;//current annuncements
          anns.push(result.data);//add new announcements to the list
          $('#add-ann-btn').trigger('click');
          this.setState({ add: false, anns: anns });
          //console.log(anns);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  annForm = () => {
    return (
      <form>
        <label>Title</label>
        <input
          type="text"
          className="form-control mb-2"
          name="title"
          value={this.state.title}
          required={true}
          onChange={this.handleChange}
        />
        <div className="mb-2">
          <label>Message</label>
          <TextArea
            name="text"
            value={this.state.text}
            onChange={this.handleEditorChange}
          />
        </div>
        <div className="row mt-1">
          <div className="col">
            <div className="form-group form-check">
              <input
                type="checkbox"
                className="form-check-input"
                name="is_published"
                value={this.state.is_published}
                onChange={this.handleChange}
                id="is_published"
              />
              <label className="form-check-label" htmlFor="is_published">
                Publish
              </label>
            </div>
          </div>
        </div>
        <button
          type="button"
          onClick={this.handleSubmit}
          className="btn btn-primary mb-4"
        >
          Submit
        </button>
      </form>
    );
  };

  addBtnBlock = () => {
    return (
      <React.Fragment>
        <div className="card mt-2 toggle">
          <div
            id="add-ann-btn"
            className="card-body"
            data-toggle="collapse"
            href="#ann-form"
            aria-expanded="false"
            aria-controls="add-form"
            onClick={() => {
              this.setState({
                plus: this.state.plus === "plus" ? "minus" : "plus",
              });
            }}
          >
            <i className={`fa fa-${this.state.plus}`}></i> Add
          </div>
        </div>
        <div className="collapse card" id="ann-form">
          <div className="card-body">{this.annForm()}</div>
        </div>
      </React.Fragment>
    );
  };

  render() {
    var annHtml = "";
    if (this.state.anns.length) {
      annHtml = this.state.anns.map((value, i) => (
        <DispalyAnnouncement value={value} key={i} />
      ));
    }
    return (
      <div>
        <h1 className='header ml-1'>Updates</h1>
        {annHtml}
        {this.addBtnBlock()}
      </div>
    );
  }
}
export default Announcements;

import React from "react";
import axios from "axios";
import DisplayCenter from "../components/helpers/displayRefugeeCentre";
class RefugeeCenters extends React.Component {
  constructor(props) {
    super(props);
    this.state = { centers: [], error: 0 };
  }

  componentDidMount() {
    let API_URL = process.env.API_URL;
    axios
      .get(`${API_URL}/content/centers`)
      .then((cs) => {
        this.setState({ centers: cs.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  componentWillUnmount() {}

  render() {
    if (!this.state.centers.length) {
      return <div />;
    }
    var html = "";
    html = this.state.centers.map((c, k) => (
      <DisplayCenter key={k} center={c} />
    ));
    return <div className="flex">{html}</div>;
  }
}

export default RefugeeCenters;

import React from "react";
import parseHtml from "html-react-parser";
import TextEditor from "../components/helpers/textareaEditor";
import DisplayDate from "../components/helpers/displayDate";
import axios from "axios";
class Question extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      iniData: this.props.question,
      toggle: false,
      edit: false,
      is_published: this.props.question.is_published,
      question: this.props.question.question,
      answer: this.props.question.answer,
      updated_at: this.props.question.updated_at,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleTextEditorChange = this.handleTextEditorChange.bind(this);
    this.editForm = this.editForm.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }
  handleTextEditorChange = (value) => {
    this.setState({ answer: value });
  };
  handleChange = (e) => {
    var target = e.target;
    var name = target.name;
    var value = target.type === "checkbox" ? target.checked : target.value;
    this.setState({ [name]: value });
  };
  handleCancel = () => {
    this.setState({
      id: this.state.iniData.id,
      question: this.state.iniData.question,
      answer: this.state.iniData.answer,
      is_published: this.state.iniData.is_published,
      edit: false
    });
  };
  handleSubmit = () => {
    let API_URL = process.env.API_URL;
    let dataAfter = {
      id: this.state.iniData.id,
      question: this.state.question,
      answer: this.state.answer,
      is_published: this.state.is_published,
    };
    if (
      this.state.dataAfter ===
      {
        id: this.state.iniData.id,
        question: this.state.iniData.question,
        answer: this.state.iniData.answer,
        is_published: this.state.iniData.is_published,
      }
    ) {
      this.setState({ edit: false });
      return;
    }
    axios
      .post(`${API_URL}/question/save`, dataAfter) //handle create and update
      .then((q) => {
        var q = q.data.question;
        console.log(q.id);
        this.setState({
          is_published: q.is_published,
          question: q.question,
          answer: q.answer,
          edit: false,
          updated_at: q.updated_at,
          iniData: {
            id: q.id,
            question: q.question,
            answer: q.answer,
            is_published: q.is_published,
            updated_at: q.updated_at,
          },
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  editForm = () => {
    return (
      <form>
        <label>Question</label>
        <input
          type="text"
          className="form-control mb-2"
          name="question"
          value={this.state.question}
          onChange={this.handleChange}
        />
        <label>Answer</label>
        <TextEditor
          name="answer"
          value={this.state.answer}
          onChange={this.handleTextEditorChange}
        />
        <div className="form-group form-check mt-2">
          <input
            type="checkbox"
            className="form-check-input"
            name="is_published"
            value={this.state.is_published}
            onChange={this.handleChange}
            checked={this.state.is_published}
            id="is_published"
          />
          <label className="form-check-label" htmlFor="is_published">
            Publish
          </label>
        </div>
        <div className="btn-group mt-2" role="group">
          <button
            type="button"
            onClick={this.handleSubmit}
            className="btn btn-success"
          >
            <i className="fa fa-save"></i>
          </button>
          <button
            type="button"
            onClick={this.handleCancel}
            className="btn btn-danger"
          >
            <i className="fa fa-close"></i>
          </button>
        </div>
      </form>
    );
  };

  render() {
    var id = this.state.iniData.id;
    return (
      <div>
        <li
          className="list-group-item mb-3 bg-danger text-white"
          data-toggle="collapse"
          href={"#collapse-" + id}
          role="button"
          aria-expanded="false"
          aria-controls={"collapse-" + id}
        >
          <div className="row">
            <div
              className="col"
              onClick={() => this.setState({ toggle: !this.state.toggle })}
            >
              {this.state.question}{" "}
              <div className="float-right small">
                <DisplayDate date={this.state.updated_at} />
              </div>
            </div>
            {this.state.toggle ? (
              <i
                className="fa fa-chevron-up float-right mr-2"
                onClick={() => this.setState({ toggle: !this.state.toggle })}
              ></i>
            ) : (
              <i
                className="fa fa-chevron-down float-right mr-2"
                onClick={() => this.setState({ toggle: !this.state.toggle })}
              ></i>
            )}
          </div>
        </li>
        <div className="collapse p-2" id={"collapse-" + id}>
          <div className="card">
            <div className="card-body">
              {this.state.edit ? (
                this.editForm()
              ) : (
                <div>
                  {parseHtml(this.state.answer ? this.state.answer : " ")}
                  <div
                    onClick={() => {
                      this.setState({ edit: true });
                    }}
                  >
                    <span className='small'>Published {this.state.is_published ? <i className='fa fa-check text-success'></i>: <i className='fa fa-close text-danger'></i>}</span>
                    {this.state.answer ? (
                      <i className="fa fa-pencil toggle float-right p-2 small"></i>
                    ) : (
                      <span className="float-right toggle">
                        <i className="fa fa-plus"></i> Add answer
                      </span>
                    )}
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Question;

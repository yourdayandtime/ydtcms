import React from "react";
import axios from "axios";
const $ = require("jquery");
$.DataTable = require("datatables.net");
require("datatables.net-buttons-dt");
require("datatables.net-buttons/js/buttons.html5.js");
require("datatables.net-buttons/js/buttons.print.js");
require("datatables.net-select-dt");

const columns = [
  { title: "Name", data: "fullName" },
  { title: "Gender", data: "genderString" },
  { title: "Permit type", data: "permit_type" },
  { title: "Permit no", data: "permit_no" },
  { title: "Country", data: "country" },
];
class SelectedDataTable extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    console.log(this.props.listId);
    var url = `${process.env.API_URL}/selectedUsers/latest`;
    if (this.props.listId) {
      url = `${process.env.API_URL}/selectedUsers/findByField/id/${this.props.listId}`;
    }
    //To do: find way to avoid multiple requests of same data from the server/api and reuse previously received data
    axios
      .get(url)
      .then((users) => {
        var data = users.data.rows;
        console.log(data);
        $("#list-table-wrapper").find("div.list-title").text(users.data.title);
        $("#list-table-wrapper div.list-desc").text(users.data.reason);
        $("#selectedUsers_table").data("listId", users.data.id);
        $("#selectedUsers_table").DataTable({
          //dom: "Bfrtip",//required to display
          dom: 'B<"clear">lfrtip', // this allows to show entries select box to display when other buttons dispay
          data: data,
          columns,
          stateSave: true, //keep current data on reload
          buttons: [
            "print",
            "pdf",
            {
              text: "Publish",
              action: function () {
                alert($("#selectedUsers_table").data("listId"));
              },
            },
            {
              text: "Publish & Send SMS",
              action: function () {
                alert($("#selectedUsers_table").data("listId"));
              },
            },
          ],
          select: { items: "multi" },
        });
        //keep selected rows  highlighted
        $("#selectedUsers_table tbody").on("click", "tr", function () {
          $(this).toggleClass("selected");
        });
        $("ul.list-menus").on("click", "li", function (e) {
          console.log($(this).data("id"));
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  componentWillUnmount() {
    $("#selectedUsers_table_wrapper").find("table").DataTable().destroy(true);
  }
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <div className="col-md-9 mx-auto">
        <div className="mt-1 mt-2"></div>
        <div className="card-body">
          <div id="list-table-wrapper">
            <div class="card">
              <div className="card-header list-title bg-danger text-white"></div>
              <div class="card-body list-desc bg-info text-white"></div>
            </div>
            <table id="selectedUsers_table" className="display table" />
          </div>
        </div>
      </div>
    );
  }
}

export default SelectedDataTable;
